﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Card
    {
        public string Suit { get; set; }
        public byte Value { get; set; }
        public string Name { get; set; }
        public bool Ace { get; set; }
        
    }
}
